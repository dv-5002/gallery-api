package main

import (
	"gallery-api/config"
	"gallery-api/handlers"
	"gallery-api/hash"
	"gallery-api/middleware"
	"gallery-api/models"
	"log"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	db.LogMode(true)

	if conf.Mode == "dev" {
		gin.SetMode(gin.ReleaseMode) // dev only!
	}

	err = models.AutoMigrate(db)
	if err != nil {
		log.Fatal(err)
	}

	hmac := hash.NewHMAC(conf.HMACKey)
	gs := models.NewGalleryService(db)
	ims := models.NewImageService(db)
	us := models.NewUserService(db, hmac)

	// gh := handlers.NewGalleryHandler(gs)
	// imh := handlers.NewImageHandler(gs, ims)
	// uh := handlers.NewUserHandler(us)
	s := handlers.NewHandler(gs, us, ims)

	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	r.Static("/upload", "./upload")

	r.POST("/signup", s.Signup)
	r.POST("/signin", s.Signin)
	r.GET("/galleries", s.ListPublish)
	r.GET("/galleries/:id", s.GetOne)
	r.GET("/galleries/:id/images", s.ListGalleryImages)
	auth := r.Group("/")
	auth.Use(middleware.RequireUser(us))
	{
		auth.POST("/logout", s.Logout)
		admin := auth.Group("/admin")
		{
			admin.POST("/galleries", s.Create)
			admin.GET("/galleries", s.List)
			admin.GET("/galleries/:id", s.Check)
			admin.DELETE("/galleries/:id", s.Delete)
			admin.PATCH("/galleries/:id/names", s.UpdateName)
			admin.PATCH("/galleries/:id/publishes", s.UpdatePublishing)
			admin.POST("/galleries/:id/images", s.CreateImage)
			admin.GET("/galleries/:id/images", s.ListGalleryImages)
			admin.DELETE("/images/:id", s.DeleteImage)
			admin.GET("/profile", s.GetProfile)
			admin.PATCH("/profile/", s.UpdateProfile)
			admin.GET("/profile/:id", s.GetUserByID)
		}
	}
	r.Run(":8080")
}
