package handlers

import (
	"fmt"
	"gallery-api/context"
	"gallery-api/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// type Handler struct {
// 	gs models.GalleryService
// }

// func NewHandler(gs models.GalleryService) *Handler {
// 	return &Handler{gs}
// }

type GalleryRes struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	IsPublish bool      `json:"is_publish"`
	Images    []ImageRes `json:"images"`
	Owner     User      `json:"owner"`
	CreateAt  time.Time `json:"createAt"`
	UpdateAt  time.Time `json:"updateAt"`
}

type CreateReq struct {
	Name string `json:"name"`
}

type CreateRes struct {
	GalleryRes
}

func (s *Handler) Create(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	req := new(CreateReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	gallery := new(models.Gallery)
	gallery.Name = req.Name
	gallery.UserID = user.ID
	if err := s.gs.Create(gallery); err != nil {
		Error(c, 500, err)
		return
	}
	res := new(CreateRes)
	res.ID = gallery.ID
	res.Name = gallery.Name
	res.IsPublish = gallery.IsPublish
	c.JSON(201, res)
}

func (s *Handler) ListPublish(c *gin.Context) {
	data, err := s.gs.ListAllPublish()
	if err != nil {
		Error(c, 500, err)
		return
	}
	galleries := []GalleryRes{}
	for _, d := range data {
		images := []ImageRes{}
		for _, img := range d.Images {
			images = append(images, ImageRes{
				ID:        img.ID,
				GalleryID: img.GalleryID,
				Filename:  img.URLPath(),
			})
		}
		galleries = append(galleries, GalleryRes{
			ID:        d.ID,
			Name:      d.Name,
			IsPublish: d.IsPublish,
			Images:    images,
		})
	}
	c.JSON(200, galleries)
}

func (s *Handler) List(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := s.gs.ListByUserID(user.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	galleries := []GalleryRes{}
	for _, d := range data {
		user, err := s.us.GetByID(d.UserID)
		if err != nil {
			Error(c, 500, err)
			return
		}
		galleries = append(galleries, GalleryRes{
			ID:        d.ID,
			Name:      d.Name,
			IsPublish: d.IsPublish,
			CreateAt:  d.CreatedAt,
			UpdateAt:  d.UpdatedAt,
			Owner: User{
				ID:    user.ID,
				Email: user.Email,
				Firstname:  user.Firstname,
				Lastname:  user.Lastname,
			},
		})
	}
	c.JSON(200, galleries)
}

func (s *Handler) GetOne(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	data, err := s.gs.GetByID(uint(id))
	if err != nil {
		Error(c, 500, err)
		return
	}
	user, err := s.us.GetByID(data.UserID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	if data.IsPublish == true {
		fmt.Println("true")
		c.JSON(200, GalleryRes{
			ID:        data.ID,
			Name:      data.Name,
			IsPublish: data.IsPublish,
			CreateAt:  data.CreatedAt,
			UpdateAt:  data.UpdatedAt,
			Owner: User{
				ID:    user.ID,
				Email: user.Email,
				Firstname:  user.Firstname,
				Lastname:  user.Lastname,
			},
		})
	} else {
		fmt.Println("false")
		c.Status(401)
	}
}
func (s *Handler) Check(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	data, err := s.gs.GetByID(uint(id))
	if err != nil {
		Error(c, 500, err)
		return
	}
	if data.UserID != user.ID {
		c.JSON(401, gin.H{
			"status":  false,
			"message": "not owner",
		})
		return
	} else {
		c.JSON(200, GalleryRes{
			ID:        data.ID,
			Name:      data.Name,
			IsPublish: data.IsPublish,
			CreateAt:  data.CreatedAt,
			UpdateAt:  data.UpdatedAt,
			Owner: User{
				ID:    user.ID,
				Email: user.Email,
				Firstname:  user.Firstname,
				Lastname:  user.Lastname,
			},
		})
	}
}
func (s *Handler) Delete(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	err = s.gs.DeleteGallery(uint(id))
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}

type UpdateNameReq struct {
	Name string `json:"name"`
}

func (s *Handler) UpdateName(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	req := new(UpdateNameReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	err = s.gs.UpdateGalleryName(uint(id), req.Name)
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}

type UpdateStatusReq struct {
	IsPublish bool `json:"is_publish"`
}

func (s *Handler) UpdatePublishing(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	err = s.gs.UpdateGalleryPublishing(uint(id), req.IsPublish)
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}
