package handlers

import (
	"errors"
	"gallery-api/context"
	"gallery-api/header"
	"gallery-api/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// type UserHandler struct {
// 	us models.UserService
// }

// func NewUserHandler(us models.UserService) *UserHandler {
// 	return &UserHandler{us}
// }

type User struct {
	ID        uint   `json:"id"`
	Email     string `json:"email"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

type SignupReq struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

func (s *Handler) Signup(c *gin.Context) {
	req := new(SignupReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	user.Firstname = req.Firstname
	user.Lastname = req.Lastname
	if err := s.us.Create(user); err != nil {
		Error(c, 500, err)
		return
	}
	c.JSON(201, gin.H{
		"token":     user.Token,
		"email":     user.Email,
		"firstname": user.Firstname,
		"lastname":  user.Lastname,
	})
}

type SigninReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (s *Handler) Signin(c *gin.Context) {
	req := new(SigninReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	token, err := s.us.Signin(user)
	if err != nil {
		Error(c, 401, err)
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (s *Handler) Logout(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		Error(c, 401, errors.New("invalid token"))
		return
	}
	err := s.us.Logout(user)
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}

func (s *Handler) GetProfile(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		Error(c, 401, errors.New("invalid token"))
		return
	}
	c.JSON(200, gin.H{
		"id":        user.ID,
		"email":     user.Email,
		"firstname": user.Firstname,
		"lastname":  user.Lastname,
	})
}

type UpdateProfileReq struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

func (s *Handler) UpdateProfile(c *gin.Context) {
	token := header.GetToken(c)
	user, err := s.us.GetByToken(token)
	if err != nil {
		c.Status(401)
		c.Abort()
		return
	}

	inputUser := context.User(c)
	if user == nil {
		Error(c, 401, errors.New("invalid token"))
		return
	}

	err = s.us.UpdateProfile(token, inputUser.Firstname, inputUser.Lastname)
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}

func (s *Handler) GetUserByID(c *gin.Context) {
	token := header.GetToken(c)
	user, err := s.us.GetByToken(token)
	if err != nil {
		c.Status(401)
		c.Abort()
		return
	}
	// strID := c.Param("id")
	// id, err := strconv.Atoi(strID)
	// user, err := s.us.GetByID(uint(id))
	// if err != nil {
	// 	c.JSON(404, gin.H{
	// 		"message": err.Error(),
	// 	})
	// }
	c.JSON(http.StatusOK, user)
}
